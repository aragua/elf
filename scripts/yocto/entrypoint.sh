#!/bin/bash -e

set -e

elf-welcome.sh

while getopts ":c" o; do
    case "${o}" in
        c)
            config=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

export ELF_ENV_TYPE=$(manifest.py default --tag type --basic-output)

if [ -z "${config}" ]
then
    export ELF_ENV_CONFIG="$(manifest.py config --tag name --basic-output | head -n1)"
    echo "Using default config from manifest : ${ELF_ENV_CONFIG}"
else
    export ELF_ENV_CONFIG="${config}"
fi

fetch.sh

oe-init.sh $ELF_ENV_CONFIG

source layers/poky/oe-init-build-env build/$ELF_ENV_CONFIG/

exec $@