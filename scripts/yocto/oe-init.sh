#!/usr/bin/bash

# Usage oe-init.sh config

set -e

MANIFEST_PATH=".manifest.xml"
WORKSPACE_PATH="$PWD"
PROJECT_CONF_PATH=

YOCTO_CONFIG="$1"
if [ -z $YOCTO_CONFIG ]
then
    YOCTO_CONFIG=$(manifest.py config --tag name --basic-output | head -1)
    if [ -z $YOCTO_CONFIG ]
    then
        YOCTO_CONFIG="noconfig"
    fi
fi


echo "#####     Init Yocto environment $YOCTO_CONFIG    #####"

initenv=$(find . -path */*/poky/oe-init-build-env)
builddir="build/$YOCTO_CONFIG"

mkdir -p $builddir

source $initenv $builddir > /dev/null

cp /opt/elf/default/yocto/templateconf/local.conf.sample conf/local.conf
manifest.py --manifest=../../$MANIFEST_PATH config[@name=\'$YOCTO_CONFIG\']/project-var project-var --basic-output | sed 's/^project-var.//' > conf/project.conf

layers=$(manifest.py --manifest=../../$MANIFEST_PATH config[@name=\'$YOCTO_CONFIG\']/layer layer --tag path --basic-output | sed -e "s|^|$WORKSPACE_PATH/|" | xargs )

bitbake-layers add-layer $layers

exit 0