#!/usr/bin/bash

set -e

echo "Deploying $ELF_ENV_CONFIG"

deployscript="../../$(manifest.py --manifest=../../.manifest.xml config[@name=\'$ELF_ENV_CONFIG\']/deploy --tag script --basic-output)"

if [ ! -f $deployscript ]
then
    echo "Deploy script ($deployscript) not found"
    exit 1
fi

$deployscript $@

exit 0