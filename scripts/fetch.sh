#!/usr/bin/bash

set -e

echo '#####     Fetch sources     #####'

default_branch=$(manifest.py default --tag revision --basic-output)
if [ -z "$default_branch" ]
then
    default_branch="master"
fi

for pn in $(manifest.py project --tag name --basic-output)
do
    just_created=0
    path=$(manifest.py project[@name=\'$pn\'] --tag path --basic-output)
    revision=$(manifest.py project[@name=\'$pn\'] --tag revision --basic-output)
    branch=$(manifest.py project[@name=\'$pn\'] --tag branch --basic-output)
    if [ -z "$branch" ]
    then
        branch=$default_branch
    fi
    # if the layer doesn't exists, we clone it
    if ! [ -d $path ]
    then
        remote=$(manifest.py project[@name=\'$pn\'] --tag remote --basic-output)
        remote_url=$(manifest.py remote[@name=\'$remote\'] --tag fetch --basic-output)
        nogitext=$(manifest.py project[@name=\'$pn\'] --tag nogitext --basic-output)
        if [ x"$nogitext" == "xyes" ]
        then
            ext=""
        else
            ext=".git"
        fi
        git clone $remote_url/$pn$ext $path -b $branch
        # checkout the revision if specified
        if ! [ -z "$revision" ]
        then
            cd $path
            git reset --hard $revision
            cd - > /dev/null
        fi
    fi

    # Once the layer exists, we check that we use the correct revision
    if ! [ -z "$revision" ]
    then
        cd $path
        current_revision=$(git log --format="%H" -n 1)
        if [ "$revision" != "$current_revision" ]
        then
            echo "The current revision ($current_revision) differ from the expected one ($revision)"
            git status
        fi
        cd - > /dev/null
    else
        cd $path
        current_branch=$(git branch --show-current)
        if [ "$branch" != "$current_branch" ]
        then
            echo "The current branch ($current_branch) differ from the expected one ($branch)"
        fi
        cd - > /dev/null
    fi
done