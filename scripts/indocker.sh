#!/usr/bin/bash

if ! $(cat /proc/1/sched | head -n 1 | grep -q -e init -e systemd )
then
    echo yes
else
    echo no
fi