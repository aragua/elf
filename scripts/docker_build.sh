#!/usr/bin/bash

set -e

ELFDIR="$(dirname $(realpath $0))/../"

PROJECT_TYPE="$1"
if ! [ -f "$ELFDIR/default/$PROJECT_TYPE/Dockerfile" ]
then
    echo "Error: unknown project type ($1)"
    exit 1
fi

if ! docker image ls | grep -q "elf-$PROJECT_TYPE"
then
    echo "Building elf-$PROJECT_TYPE" 
    docker build -t "elf-$PROJECT_TYPE" "$ELFDIR/default/" -f "$ELFDIR/default/$PROJECT_TYPE/Dockerfile"
fi

exit 0