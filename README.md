# Embedded linux factory

This tool aims at simplifying linux development and integration.
It simplify the use of docker to work on a project.
The user environment is duplicated inside the docker but the environment and tools are set to the project need.
It optionnaly uses a manifest file to fetch sources using git.

## Dependencies

docker, git, python3 python3-lxml

## How to use ?

Clone this software in a directory.
Call it like this:

```
$ cd project_dir
$ <path>/elf.py
```

## Usage

elf have to be used with subcommands like:
- manifest
- docker
- run
- deploy

Use elf <subcommand> help for more informations

## Get extra sources from manifest

You can define a file named .manifest.xml to retireve sources as google repo does.

### format

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>

  <default sync-j="4" revision="langdale"/>

  <remote fetch="https://github.com/openembedded" name="oe"/>
  <project remote="oe" name="meta-openembedded" path="layers/meta-openembedded" sublayers="meta-oe,meta-python"/>
  <layer path="layers/meta-openembedded/meta-oe"/>
  <layer path="layers/meta-openembedded/meta-python"/>

  <remote fetch="https://git.yoctoproject.org/git" name="yocto"/>
  <project remote="yocto" name="poky" path="layers/poky"/>
  <project remote="yocto" name="meta-raspberrypi" path="layers/meta-raspberrypi"/>

  <remote fetch="git@gitlab.com:aragua" name="aragua" />
  <project remote="aragua" name="meta-misc" path="layers/meta-misc" revision="1a21eae305119aff5dcb20dbd4316e738f2c2ca8" />
  <layer path="layers/meta-misc"/>

  <config name="rpi" >
    <layer path="layers/meta-raspberrypi"/>
    <project-var MACHINE="raspberrypi3-64" DISTRO="misc" ENABLE_UART="1" />
    <deploy script="layers/meta-cpuaux" >
  </config>


</manifest>
```

### manifest

This label is mandatory and unique to encapsulate datas.

### default

Define default behaviour

#### revision

Default branch to use if none is specified in a project

#### type

Specify the kind of project (see known project type)

### remote

As for google repo, this label is used to define a remote server containing sources;

#### name

Name of the remote used as a reference by other labels

#### fetch

Url of the remote

### project

Repository to clone in order to get sources

#### name

name of the repository on the remote server

#### remote

Reference to the remote url

#### path

Destination where to clone the repository

#### revision (Optional)

Revision to use for that repository

#### branch (Optional)

Branch to use with that repository if revision is not set

### layer (Optional,Yocto)

Project type specific label to define what layers to use.

### config (Optional,Yocto)

Define a specific configuration to set

#### project-var (Optional,Yocto)

Variable to set in build/<config>/conf/project.conf
This configuration file is added to the default local.conf

#### layer (Optional,Yocto)

Project type specific label to define what layers is used by that configuration.

#### deploy (Optional,Yocto)

Information to deploy images on that machine.