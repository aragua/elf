#!/usr/bin/env python3

import getpass
import os
import platform
import re
import subprocess
import sys
from argparse import (
    Action,
    ArgumentParser,
    ArgumentTypeError,
    Namespace,
    RawDescriptionHelpFormatter,
)
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, List, Literal, Optional, Sequence, Union

import scripts.manifest as mf

version="0.0.1"
verbose=False
elftopdir=os.path.dirname(os.path.realpath(__file__)) 

@dataclass
class DotFile:
    file_name: str
    mount_opt: Optional[str] = None


DOT_FILES_MOUNT: List[DotFile] = [
    DotFile(".bash_history"),
    DotFile(".gitconfig", "ro"),
    DotFile(".inputrc", "ro"),
    DotFile(".ssh", "ro"),
]


def existing_dir_path(value: str):
    path = Path(value)
    if path.exists() and path.is_dir():
        return path
    else:
        raise ArgumentTypeError(
            f'"{path.absolute()}" is not an existing directory path.'
        )


class MultipleArgumentAction(Action):
    def __init__(
        self,
        option_strings: Sequence[str],
        dest: str,
        values: Dict[str, Any],
        nargs: Union[int, str, None] = None,
        **kwargs,
    ) -> None:
        self.values = values
        super().__init__(option_strings, dest, nargs, **kwargs)

    def __call__(
        self,
        parser: ArgumentParser,
        namespace: Namespace,
        values: Union[str, Sequence[Any], None],
        option_string: Union[str, None] = None,
    ) -> None:
        for dest, default_value in self.values.items():
            setattr(
                namespace, dest, default_value if values is None else values
            )


class OptionalValueAction(Action):
    def __init__(
        self,
        option_strings: Sequence[str],
        dest: str,
        default_value: Any,
        nargs: Union[int, Literal["*", "+", "?", "..."], None] = None,
        **kwargs,
    ) -> None:
        if nargs not in ["?", "*"]:
            raise ValueError(f"Invalid nargs value: {nargs}")

        self.default_value = default_value

        super().__init__(option_strings, dest, nargs, **kwargs)

    def __call__(
        self,
        parser: ArgumentParser,
        namespace: Namespace,
        values: Union[str, Sequence[Any], None],
        option_string: Union[str, None] = None,
    ) -> None:
        setattr(
            namespace,
            self.dest,
            self.default_value if values is None else values,
        )


def volume_cmd(
    local_path: Path, container_path: str, opt: Optional[str] = None
) -> List[str]:
    cmd = (
        [
            "--volume",
            f"{local_path.absolute()}:{container_path}"
            f'{f":{opt}" if opt else ""}',
        ]
        if local_path.exists()
        else []
    )

    return cmd


def mount_dot_file_cmd(file: DotFile, home_path: str) -> List[str]:
    return volume_cmd(
        Path.home().joinpath(file.file_name),
        f"{home_path}/{file.file_name}",
        file.mount_opt,
    )


def cmd_line(namespace: Namespace, additional_args: List[str]) -> List[str]:
    docker_image_pattern = re.compile(r"(?P<image_name>[^:]+)(:.+)?$")
    username = getpass.getuser()
    docker_image_groups = docker_image_pattern.search(f"elf-{namespace.type}")
    assert docker_image_groups is not None
    container_hostname = (
        f"{docker_image_groups['image_name']}.{platform.node()}"
    )

    cmd = ["docker", "run", "--interactive", "--tty"]

    if namespace.persistent is False:
        cmd += ["--rm"]

    if sys.platform == "win32":
        container_workdir = "/workdir"
    else:
        container_workdir = namespace.workdir
        cmd += [
            "--env",
            f"MP_UID={os.getuid()}",
            "--env",
            f"MP_GID={os.getgid()}",
        ]

    cmd += volume_cmd(Path(f"{os.path.dirname(__file__)}/"), f"/opt/elf/")

    cmd += volume_cmd(Path(f"/home/{username}/.elf/"), f"/share/")

    if namespace.scriptdir:
        cmd += volume_cmd(Path(f"{namespace.scriptdir}"), f"/home/{username}/scripts/")

    if namespace.http_proxy_url:
        cmd += ["--env", f"HTTP_PROXY={namespace.http_proxy_url}"]
    if namespace.https_proxy_url:
        cmd += ["--env", f"HTTPS_PROXY={namespace.https_proxy_url}"]

    cmd += [
        "--env",
        f"MP_USER={username}",
        "--volume",
        f"{namespace.workdir}:{container_workdir}",
        "--workdir",
        f"{namespace.cd_path or container_workdir}",
        "--label",
        f'"user={username},workdir={namespace.workdir}"',
        "--hostname",
        f"{container_hostname}",
    ]

    if os.path.isfile(f"{elftopdir}/scripts/{namespace.type}/entrypoint.sh"):
        entrypoint = f"ENTRYPOINT=/opt/elf/scripts/{namespace.type}/entrypoint.sh"
    else:
        entrypoint = f"ENTRYPOINT=/opt/elf/scripts/entrypoint.sh"

    cmd += [ "--env", entrypoint, ]

    if namespace.mount_dot_files:
        for dot_file in DOT_FILES_MOUNT:
            cmd += mount_dot_file_cmd(dot_file, f"/home/{username}")

    cmd += additional_args

    cmd += [f"elf-{namespace.type}"]

    if namespace.cmd:
        cmd += namespace.cmd
    else:
        cmd += ['bash']

    if namespace.deploy_args:
        cmd += [namespace.deploy_args]

    #print(cmd)

    return cmd


def write_manifest(type: str):
    f = open(".manifest.xml", "w")
    f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
    f.write("<manifest>\n")
    f.write("<default type=\"" + type + "\" />\n")  
    f.write("</manifest>\n")
    f.close()


def build_docker_image(type: str):
    subprocess.call(os.path.dirname(__file__) + "/scripts/docker_build.sh " + type, shell=True)


def run_interactive(cmd: Sequence[str]) -> int:
    return subprocess.call(" ".join(cmd), shell=True)


def main():
    command=sys.argv[0]

    parser = ArgumentParser(
        description="Create and run a new container from an image.",
        formatter_class=RawDescriptionHelpFormatter,
        epilog='The additional arguments are forwarded to "docker run",\n'
        'see "docker run --help" for more details.',
    )
    parser.add_argument(
        "--verbose",
        dest="verbose",
        action="store_true",
        help="Make elf more verbose",
    )

    namespace, args = parser.parse_known_args()
    verbose = namespace.verbose

    if verbose is True:
        print(f"\nElf version: {version}\n")

    if len(args) == 0:
        print("Error: Elf need a subcommand (manifest, docker, run, deploy)")
        sys.exit(-1)
    subcommand=args[0]

    subparser = parser.add_subparsers(dest='subparser_name')

    if subcommand == "manifest":
        # manifest parser
        parser_manifest = subparser.add_parser(name='manifest', help='init help')
        parser_manifest.add_argument(
            "-c",
            "--create",
            dest="create",
            action="store_true",
            help="Create a new manifest file",
        )
        parser_manifest.add_argument(
            "-t",
            "--type",
            dest="type",
            default="raw",
            choices=['raw','yocto'],
            help="Project type (raw|yocto)",
        )
        subnamespace, subargs = parser_manifest.parse_known_args(args)

        if subnamespace.create is True:
            if os.path.exists(".manifest.xml") is True:
                print(".manifest.xml exists")
            else:
                write_manifest(subnamespace.type)
        else:
            os.system("cat .manifest.xml")
            print()
        
        sys.exit(0)

    if subcommand == "docker":
        parser_docker = subparser.add_parser(name='build', help="build help")
        parser_docker.add_argument(
            "-b",
            "--build",
            dest="build",
            action="store_true",
            help="Build docker image",
        )
        parser_docker.add_argument(
            "-c",
            "--clean",
            dest="clean",
            action="store_true",
            help="Clean docker image",
        )
        parser_docker.add_argument(
            "-s",
            "--show",
            dest="show",
            action="store_true",
            help="Show docker image",
        )
        parser_docker.add_argument(
            "-t",
            "--type",
            dest="type",
            required=True,
            choices=['raw','yocto'],
            help="Project type (raw|yocto). Define docker image name",
        )
        subnamespace, subargs = parser_docker.parse_known_args(args)

        if subnamespace.clean is True:
            os.system("docker image rm elf-" + subnamespace.type)

        if subnamespace.build is True:
            build_docker_image(subnamespace.type)

        if subnamespace.show is True:
            os.system("docker image ls elf-" + subnamespace.type)
        sys.exit(0)

    # Get project type and add it to the namespace or exit
    if not os.path.exists(".manifest.xml"):
        print("Error: Not an elf directory. Use elf.py manifest to init directory")
        sys.exit(-1)
    else:
        projecttype=mf.manifest_main(["default", "--tag", "type", "--basic-output"])
        # Build docker image if not built before
        build_docker_image(projecttype)

    # run and deploy parser
    if subcommand == "run" or subcommand == "deploy":
        parser_run = subparser.add_parser(name=f"{subcommand}", help=f"{subcommand} help")
        parser_run.add_argument(
            "-c",
            "--config",
            metavar="config",
            dest="config",
            help="Configuration to use",
        )
        parser_run.add_argument(
            "-D",
            "--cd",
            metavar="path",
            dest="cd_path",
            help="Working directory inside the container",
        )
        parser_run.add_argument(
            "--no-rm",
            "-R",
            dest="persistent",
            action="store_true",
            help="Start a persistent docker container",
        )
        parser_run.add_argument(
            "--no-dot-files",
            "-n",
            dest="mount_dot_files",
            action="store_false",
            help="Do not mount user dot files from home",
        )
        parser_run.add_argument(
            "-e",
            "--exec",
            dest="cmd",
            nargs="...",
            help="Command line executed inside the docker container",
        )
        parser_run.add_argument(
            "-s",
            "--scriptdir",
            metavar="scriptdir",
            dest="scriptdir",
            help="Script directory",
        )
        parser_run.add_argument(
            "-w",
            "--workdir",
            metavar="workdir_path",
            dest="workdir",
            default=Path.cwd(),
            type=existing_dir_path,
            help="Bind mount the workdir as a volume",
        )
        # Proxy arguments
        parser_run.add_argument(
            "--http-proxy",
            nargs="?",
            metavar="proxy_url",
            dest="http_proxy_url",
            action=OptionalValueAction,
            default_value=os.environ.get("HTTP_PROXY", None),
            help="Set the http proxy, if proxy_url is not set, "
            "uses the HTTP_PROXY environment variable",
        )
        parser_run.add_argument(
            "--https-proxy",
            nargs="?",
            metavar="proxy_url",
            dest="https_proxy_url",
            action=OptionalValueAction,
            default_value=os.environ.get("HTTPS_PROXY", None),
            help="Set the http proxy, if proxy_url is not set, "
            "uses the HTTPS_PROXY environment variable",
        )
        parser_run.add_argument(
            "--proxy",
            nargs="?",
            metavar="proxy_url",
            action=MultipleArgumentAction,
            values={
                "http_proxy_url": os.environ.get("HTTP_PROXY", None),
                "https_proxy_url": os.environ.get("HTTPS_PROXY", None),
            },
            help="Set the proxy, if proxy_url is not set, "
            "uses the HTTP_PROXY and HTTPS_PROXY environment variable",
        )
        parser_run.add_argument(
            "-a",
            "--args",
            metavar="deploy_args",
            dest="deploy_args",
            help="Deployment arguments",
        )
        subnamespace, subargs = parser_run.parse_known_args(args)
        subargs.pop(0)

        setattr(subnamespace, 'type', projecttype)
        if subcommand == "deploy":
            setattr(subnamespace, 'cmd', ['deploy.sh'])
            subargs += ['--privileged', '-v', '/dev/bus/usb:/dev/bus/usb']

        cmd = cmd_line(subnamespace, subargs)
        rc = run_interactive(cmd)
        sys.exit(rc)

    print("Unknown subcommand")
    sys.exit(0)


if __name__ == "__main__":
    main()
